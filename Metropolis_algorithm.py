#!/usr/bin/env python
#Peter Coogan - 15325377
#pcoogan@tcd.ie

import numpy as np
import scipy as sp
import matplotlib.pylab as plt
import random as rm

##########################################
'''2D Ising Model'''
##########################################
#Create a function that calculates change in the hamiltonian

def dH_2D(my_matrix, x, y, matrix_size, J, h):  
	return -(2*J*my_matrix[x,y]*(my_matrix[x, (y+1) %matrix_size] + my_matrix[x, (y-1) %matrix_size] + my_matrix[(x+1) %matrix_size, y] + my_matrix[(x-1) %matrix_size, y])) - 2*h*my_matrix[x,y]#each nearest neighbour to (x,y) needs to be accounted for
	

#The metropolis algorithm can be simulated

def Metrop_2D(my_matrix, times, matrix_size, J, h, Temp, k):
	i = 0.0
	for i in range(times):
		i+=1
		for x in range(0, matrix_size):
			for y in range(0, matrix_size):
				if (dH_2D(my_matrix, x, y, matrix_size, J, h))>0:
					#print(dH(my_matrix, x, y, matrix_size, J, h))
					#Spins flip if change is less than 0
					my_matrix[x,y] *= -1
				else:
					num = rm.random()
					if num < (np.exp(dH_2D(my_matrix, x, y, matrix_size, J, h)/(k*Temp))):
							my_matrix[x,y] *= -1
	return my_matrix

def mag_2D(my_matrix, matrix_size):
	return np.abs((np.sum(my_matrix)))/(matrix_size*matrix_size)

def mag_2D_suscept(my_matrix, matrix_size, Temp, k):
	a = (np.sum(my_matrix*my_matrix))/(matrix_size*matrix_size)
	return a
	
def energy_2D(my_matrix, matrix_size, J, h, Temp, k):
	H = -(J*my_matrix[x,y]*(my_matrix[x, (y+1) %matrix_size] + my_matrix[x, (y-1) %matrix_size] + my_matrix[(x+1) %matrix_size, y] + my_matrix[(x-1) %matrix_size, y])) - h*my_matrix[x,y]
	return H/2.0
	



	
	
	







