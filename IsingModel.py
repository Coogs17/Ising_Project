#!/usr/bin/env python
#Peter Coogan - 15325377
#pcoogan@tcd.ie

import numpy as np
import scipy as sp
import matplotlib.pylab as plt
import random as rm
import Ising_2D as twoD
import Triangular_2D as tri
import Ising_1D as oneD
import Ising_3D as threeD

#define constants
matrix_size = 50#dimensions
J = 1#exchange energy
h = 0#magnetic field
times = 100#iterations
#Temp = 1#temperature
k = 1#boltzmann

class Ising:#create a class to define lattices based on dimensions
    def __init__(self, matrix_size):
        self.matrix_size = matrix_size
        self.lattice_2D()
        self.lattice_1D()
        self.lattice_3D()
    
    def lattice_2D(self):#2D lattice
        self.lattice = np.random.choice([1.0, -1.0], (matrix_size, matrix_size))
        
    def lattice_1D(self):#1D lattice
        self.row = np.random.choice([1.0, -1.0], (matrix_size, 1))
        
    def lattice_3D(self):#3D lattice
        self.shape = np.random.choice([1.0, -1.0], (matrix_size, matrix_size, matrix_size))
   
#########################################################
'''Plotting the Image of the Matrix for varying sweeps'''
#########################################################        
        
'''
initial_plot = plt.figure().add_subplot(111)
#equilplot = plt.figure().add_subplot(111)
my_lattice = Ising(matrix_size).lattice
timesarray = np.arange(1, 10, 1)
plt.ion()
for i in (timesarray):
	alg = twoD.Metrop_2D(my_lattice, i, matrix_size, J, h, Temp, k)
	magnetism = twoD.mag_2D(alg, matrix_size)
	#print alg
	initial_plot.imshow(alg, cmap = 'hot', interpolation = 'none')
	plt.pause(0.5)
	#equilplot.plot(i, magnetism, 'bo')
	#plt.pause(0.5)
	initial_plot.set_title('Domain Formation')
	print 'number of sweeps: ' + str(i)
while True:
	#plt.pause(0.5)
	plt.pause(0.5)
'''
################################
'''2D Plots and Analysis'''
################################


magplot_2D = plt.figure().add_subplot(111)#magnetisation plot
magsusplot_2D = plt.figure().add_subplot(111)#magnetic susceptibility plot
energyplot_2D = plt.figure().add_subplot(111)#energy plot
sheatplot_2D = plt.figure().add_subplot(111)#specific heat plot
Temparray = np.arange(0.2, 4.1, 0.1)#range of temperatures
my_lattice_2D = Ising(matrix_size).lattice#calling the class and function for the specific lattice
for mag_T in Temparray:
    print 'Temperature: ' + str(mag_T)
	#metropolis algorithm		
    my_mag_matrix = twoD.Metrop_2D(my_lattice_2D, times, matrix_size, J, h, mag_T, k)
   
    'Magnetisation Plot'
	#magnetisation calculations
    m = twoD.mag_2D(my_mag_matrix, matrix_size)#magnetisation
    print 'Magnetisation: ' + str(m)
	#make a plot
    magplot_2D.plot(mag_T, m, 'ro')
  
    'Magnetic Susceptibility Plot'
	#magnetic susceptibility calculations
    m_s = twoD.mag_2D_suscept(my_mag_matrix, matrix_size, mag_T, k)
    print 'Susceptibility: ' + str(m_s)
	#make a plot
    magsusplot_2D.plot(mag_T, m_s, 'ro')
   
    'Energy Plot'
	#energy calculations
    e = twoD.energy_2D(my_mag_matrix, matrix_size, J, h, k)
    print 'Energy: ' + str(e)
	#make a plot
    energyplot_2D.plot(mag_T, e, 'ro')
    
    'Specific Heat Plot'
	#specific heat calculations
    s = twoD.sheat_2D(my_mag_matrix, matrix_size, J, h, mag_T, k)
    print 'Specific Heat: ' + str(s)
	#make a plot
    sheatplot_2D.plot(mag_T, s, 'ro')
    
    
    

'Magnetisation plot parameters'
magplot_2D.plot(mag_T, m, 'ro', label = str(matrix_size) + 'X' + str(matrix_size) + 'with 100 sweeps')
magplot_2D.set_title('2D Magnetisation versus Temperature')
magplot_2D.set_ylim(-0.1, 1.1)
magplot_2D.set_xlabel('Temperature[J/K_B]')
magplot_2D.set_ylabel('Magnetism')
magplot_2D.legend(loc = 3)

'Magnetic Susceptibility plot parameters'
magsusplot_2D.plot(mag_T, m_s, 'ro', label = str(matrix_size) + 'X' + str(matrix_size) + 'with 100 sweeps')
magsusplot_2D.set_title('2D Magnetic Susceptibility versus Temperature')
magsusplot_2D.set_ylim(-0.1, 0.5)
magsusplot_2D.set_xlabel('Temperature[J/K_B]')
magsusplot_2D.set_ylabel('Magnetic Susceptibility')
magsusplot_2D.legend(loc = 2)

'Energy plot parameters'
energyplot_2D.plot(mag_T, e, 'ro', label = str(matrix_size) + 'X' + str(matrix_size) + 'with 100 sweeps')
energyplot_2D.set_title('2D Energy versus Temperature')
energyplot_2D.set_ylim(-2.1, -0.4)
energyplot_2D.set_xlabel('Temperature[J/K_B]')
energyplot_2D.set_ylabel('Energy')
energyplot_2D.legend(loc = 4)

'Specific Heat plot parameters'
sheatplot_2D.plot(mag_T, s, 'ro', label = str(matrix_size) + 'X' + str(matrix_size) + 'with 100 sweeps')
sheatplot_2D.set_title('2D Specific Heat versus Temperature')
sheatplot_2D.set_ylim(-0.1, 0.2)
sheatplot_2D.set_xlabel('Temperature[J/K_B]')
sheatplot_2D.set_ylabel('Specific Heat')
sheatplot_2D.legend(loc = 2)


#####################################
'''Triangular Plots and Analysis'''
#####################################

'''
magplot_tri = plt.figure().add_subplot(111)#magnetisation plot
magsusplot_tri = plt.figure().add_subplot(111)#magnetic susceptibility plot
energyplot_tri = plt.figure().add_subplot(111)#energy plot
sheatplot_tri = plt.figure().add_subplot(111)#specific heat
Temparray = np.arange(0.1, 4.1, 0.1)#range of temperatures
my_lattice_tri = Ising(matrix_size).lattice#calling the class and the function for the specific lattice
for mag_T in Temparray:
    print 'Temperature: ' + str(mag_T)
	#metropolis algorithm
    my_mag_matrix = tri.Metrop_triangular(my_lattice_tri, times, matrix_size, J, h, mag_T, k)

    'Magnetisation Plot'
	#magnetisation calculations
    m = twoD.mag_2D(my_mag_matrix, matrix_size)
    print 'Magnetisation: ' + str(m)
	#make a plot
    magplot_tri.plot(mag_T, m, 'ro')
    
    'Magnetic Susceptibility Plot'
	#magnetic susceptibility calculations
    m_s = twoD.mag_2D_suscept(my_mag_matrix, matrix_size, mag_T, k)
    print 'Susceptibility: ' + str(m_s)
	#make a plot
    magsusplot_tri.plot(mag_T, m_s, 'ro')
    
    'Energy Plot'
	#energy calculations
    e = tri.energy_triangular(my_mag_matrix, matrix_size, J, h, k)
    print 'Energy: ' + str(e)
	#make a plot
    energyplot_tri.plot(mag_T, e, 'ro')
    
    'Specific Heat Plot'
	#specific heat calculations
    s = tri.sheat_triangular(my_mag_matrix, matrix_size, J, h, mag_T, k)
    print 'Specific Heat: ' + str(s)
	#make a plot
    sheatplot_tri.plot(mag_T, s, 'ro')
    

    
    
    

'Magnetisation plot parameters'
magplot_tri.plot(mag_T, m, 'ro', label = str(matrix_size) + 'X' + str(matrix_size) + 'with 100 sweeps')
magplot_tri.set_title('Triangular Magnetisation versus Temperature')
magplot_tri.set_ylim(-0.1, 1.1)
magplot_tri.set_xlabel('Temperature[J/K_B]')
magplot_tri.set_ylabel('Magnetism')
magplot_tri.legend()

'Magnetic Susceptibility plot parameters'
magsusplot_tri.plot(mag_T, m_s, 'ro', label = str(matrix_size) + 'X' + str(matrix_size) + 'with 100 sweeps')
magsusplot_tri.set_title('Triangular Magnetic Susceptibility versus Temperature')
magsusplot_tri.set_xlabel('Temperature[J/K_B]')
magsusplot_tri.set_ylabel('Magnetic Susceptibility')
magsusplot_tri.legend()

'Energy plot parameters'
energyplot_tri.plot(mag_T, e, 'ro', label = str(matrix_size) + 'X' + str(matrix_size) + 'with 100 sweeps')
energyplot_tri.set_title('Triangular Energy versus Temperature')
energyplot_tri.set_xlabel('Temperature[J/K_B]')
energyplot_tri.set_ylabel('Energy')
energyplot_tri.legend()

'Specific Heat plot parameters'
sheatplot_tri.plot(mag_T, s, 'ro', label = str(matrix_size) + 'X' + str(matrix_size) + 'with 100 sweeps')
sheatplot_tri.set_title('Triangular Specific Heat versus Temperature')
sheatplot_tri.set_xlabel('Temperature[J/K_B]')
sheatplot_tri.set_ylabel('Specific Heat')
sheatplot_tri.legend()
'''


############################
'''1D plots and Analysis'''
############################
'''

magplot_1D = plt.figure().add_subplot(111)#magnetisation plot
magsusplot_1D = plt.figure().add_subplot(111)#magnetic susceptibility plot
energyplot_1D = plt.figure().add_subplot(111)#energy plot
sheatplot_1D = plt.figure().add_subplot(111)#specific heat plot
Temparray = np.arange(0.1, 4.1, 0.1)#range of temperatures
my_lattice_1D = Ising(matrix_size).row#calling the class and the function for the specific lattice
for mag_T in Temparray:
    print 'Temperature: ' + str(mag_T)
    #metropolis algorithm
    my_mag_matrix = oneD.Metrop_1D(my_lattice_1D, times, matrix_size, J, h, mag_T, k)

    'Magnetisation Plot'
	#magnetisation calculations
    m = oneD.mag_1D(my_mag_matrix, matrix_size)
    print 'Magnetisation: ' + str(m)
	#make a plot
    magplot_1D.plot(mag_T, m, 'ro')
    
    'Magnetic Susceptibility Plot'
	#magnetic susceptibility calculations
    m_s = oneD.mag_1D_suscept(my_mag_matrix, matrix_size, mag_T, k)
    print 'Susceptibility: ' + str(m_s)
	#make a plot
    magsusplot_1D.plot(mag_T, m_s, 'ro')
    
    'Energy Plot'
	#energy calculations
    e = oneD.energy_1D(my_mag_matrix, matrix_size, J, h, k)
    print 'Energy: ' + str(e)
	#make a plot
    energyplot_1D.plot(mag_T, e, 'ro')
    
    'Specific Heat Plot'
	#specific heat calculations
    s = oneD.sheat_1D(my_mag_matrix, matrix_size, J, h, mag_T, k)
    print 'Specific Heat: ' + str(s)
	#make a plot
    sheatplot_1D.plot(mag_T, s, 'ro')
    

    
    
    

'Magnetisation plot parameters'
magplot_1D.plot(mag_T, m, 'ro', label = str(matrix_size) + 'X' + str(matrix_size) + 'with 100 sweeps')
magplot_1D.set_title('1D Magnetisation versus Temperature')
magplot_1D.set_ylim(-0.1, 1.1)
magplot_1D.set_xlabel('Temperature[J/K_B]')
magplot_1D.set_ylabel('Magnetism')
magplot_1D.legend()

'Magnetic Susceptibility plot parameters'
magsusplot_1D.plot(mag_T, m_s, 'ro', label = str(matrix_size) + 'X' + str(matrix_size) + 'with 100 sweeps')
magsusplot_1D.set_title('1D Magnetic Susceptibility versus Temperature')
magsusplot_1D.set_xlabel('Temperature[J/K_B]')
magsusplot_1D.set_ylabel('Magnetic Susceptibility')
magsusplot_1D.legend()

'Energy plot parameters'
energyplot_1D.plot(mag_T, e, 'ro', label = str(matrix_size) + 'X' + str(matrix_size) + 'with 100 sweeps')
energyplot_1D.set_title('1D Energy versus Temperature')
energyplot_1D.set_xlabel('Temperature[J/K_B]')
energyplot_1D.set_ylabel('Energy')
energyplot_1D.legend()

'Specific Heat plot parameters'
sheatplot_1D.plot(mag_T, s, 'ro', label = str(matrix_size) + 'X' + str(matrix_size) + 'with 100 sweeps')
sheatplot_1D.set_title('1D Specific Heat versus Temperature')
sheatplot_1D.set_xlabel('Temperature[J/K_B]')
sheatplot_1D.set_ylabel('Specific Heat')
sheatplot_1D.legend()
'''


#############################
'''3D Plots and Analysis'''
#############################
'''
magplot_3D = plt.figure().add_subplot(111)#magnetsation plot 
magsusplot_3D = plt.figure().add_subplot(111)#magnetic susceptibility plot
energyplot_3D = plt.figure().add_subplot(111)#energy plot
sheatplot_3D = plt.figure().add_subplot(111)#specific heat plot
Temparray = np.arange(1.2, 7.1, 0.1)#range of temperatures
my_lattice_3D = Ising(matrix_size).shape#calling the class and the specific function for the lattice
for mag_T in Temparray:
    print 'Temperature: ' + str(mag_T)
	#metropolis algorithm
    my_mag_matrix = threeD.Metrop_3D(my_lattice_3D, times, matrix_size, J, h, mag_T, k)

    'Magnetisation Plot'
	#magnetisation calculations
    m = threeD.mag_3D(my_mag_matrix, matrix_size)
    print 'Magnetisation: ' + str(m)
	#make a plot
    magplot_3D.plot(mag_T, m, 'ro')
    
    'Magnetic Susceptibility Plot'
	#magnetic susceptibility calculation
    m_s = threeD.mag_3D_suscept(my_mag_matrix, matrix_size, mag_T, k)
    print 'Susceptibility: ' + str(m_s)
	#make a plot
    magsusplot_3D.plot(mag_T, m_s, 'ro')
    
    'Energy Plot'
	#energy calculations
    e = threeD.energy_3D(my_mag_matrix, matrix_size, J, h, k)
    print 'Energy: ' + str(e)
	#make a plot
    energyplot_3D.plot(mag_T, e, 'ro')
    
    'Specific Heat Plot'
    #specific heat calculation
    s = threeD.sheat_3D(my_mag_matrix, matrix_size, J, h, mag_T, k)
    print 'Specific Heat: ' + str(s)
    #make a plot
    sheatplot_3D.plot(mag_T, s, 'ro')
    

    
    
    

'Magnetisation plot parameters'
magplot_3D.plot(mag_T, m, 'ro', label = str(matrix_size) + 'X' + str(matrix_size) + 'with 100 sweeps')
magplot_3D.set_title('3D Magnetisation versus Temperature')
magplot_3D.set_ylim(-0.1, 1.1)
magplot_3D.set_xlabel('Temperature[J/K_B]')
magplot_3D.set_ylabel('Magnetism')
magplot_3D.legend()

'Magnetic Susceptibility plot parameters'
magsusplot_3D.plot(mag_T, m, 'ro', label = str(matrix_size) + 'X' + str(matrix_size) + 'with 100 sweeps')
magsusplot_3D.set_title('3D Magnetic Susceptibility versus Temperature')
magsusplot_3D.set_xlabel('Temperature[J/K_B]')
magsusplot_3D.set_ylabel('Magnetic Susceptibility')
magsusplot_3D.legend()

'Energy plot parameters'
energyplot_3D.plot(mag_T, m, 'ro', label = str(matrix_size) + 'X' + str(matrix_size) + 'with 100 sweeps')
energyplot_3D.set_title('3D Energy versus Temperature')
energyplot_3D.set_xlabel('Temperature[J/K_B]')
energyplot_3D.set_ylabel('Energy')
energyplot_3D.legend()

'Specific Heat plot parameters'
sheatplot_3D.plot(mag_T, m, 'ro', label = str(matrix_size) + 'X' + str(matrix_size) + 'with 100 sweeps')
sheatplot_3D.set_title('3D Specific Heat versus Temperature')
sheatplot_3D.set_xlabel('Temperature[J/K_B]')
sheatplot_3D.set_ylabel('Specific Heat')
sheatplot_3D.legend()
'''

plt.show()
