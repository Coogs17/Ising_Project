
#!/usr/bin/env python
#Peter Coogan - 15325377
#pcoogan@tcd.ie

import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import random as rm

##########################
'''1D Functions'''
##########################

'''Update Nearest Neighbours'''
def line(my_matrix, matrix_size, x, y):
    #compute the nearest neighbours to [x,y]
    right = my_matrix[(x+1) %matrix_size]
    left = my_matrix[(x-1) %matrix_size]
    up = my_matrix[x,y %1]
    down = my_matrix[x,y %1]
    return right + left + up + down

'''Change in Hamiltonian'''
def dH_1D(my_matrix, x, y, matrix_size, J, h):
    return (-(2*J*my_matrix[x, y]*line(my_matrix, matrix_size, x, y))
    - 2*h*my_matrix[x, y])
    

'''Metropolis Algorithm'''
def Metrop_1D(my_matrix, times, matrix_size, J, h, Temp, k):
    i = 0.0
    
    #iterate over the number of sweeps
    for i in range(times):
        i += 1
        
        #iterate over all rows
        for x in range(0, matrix_size):
            
            
            #iterate over column in the matrix
            for y in range(0, 1):
            
                #conditions to flip a spin
                if dH_1D(my_matrix, x, y, matrix_size, J, h) > 0:
                
                    #Spins flip if change is greater than 0
                    my_matrix[x,y] *= -1
                
                else:
                
                    #create a random number between 0 and 1
                    num = rm.random()
                
                    #flip the spin if the term is greater than the number
                    if num < (np.exp((dH_1D(my_matrix, x, y, matrix_size, J, h))
                    /(k*Temp))):
                    
                        #spins flip
                        my_matrix[x,y] *= -1
                    
    
    return my_matrix


'''Magnetisation'''
def mag_1D(my_matrix, matrix_size):
    return np.abs((np.sum(my_matrix)))/(matrix_size*1)

'''Magnetic Susceptibility'''
def mag_1D_suscept(my_matrix, matrix_size, Temp, k):
    
    #square of the average of sum of all spin values
    mag1 = (mag_1D(my_matrix, matrix_size))**2
    
    #average of the sum of the square of all spin values
    mag2 = np.abs((np.sum(my_matrix*my_matrix)))/(matrix_size*1)
    
    #susceptibility
    return (mag2 - mag1)/Temp


'''Average Energy per site'''
def energy_1D(my_matrix, matrix_size, J, h, k):
    
    #initial value
    E = 0.0
    
    #iterate over all rows
    for x in range(0, matrix_size):
        
        #iterate over column
        for y in range(0, 1):
        
            #Hamiltonian
            H = (dH_1D(my_matrix, x, y, matrix_size, J, h))/2
        
            #energy is updated by half of the hamiltonian per site
            E += (0.5*H)/(matrix_size*1)
    return E        


'''Specific Heat'''
def sheat_1D(my_matrix, matrix_size, J, h, Temp, k):
    
    #initial value
    E1 = 0.0
    
    #iterate over all rows
    for x in range(0, matrix_size):
        
        #iterate over column
        for y in range(0, 1):
            
            #Hamiltonian squared
            H_squared = ((dH_1D(my_matrix, x, y, matrix_size, J, h))/2)**2
        
            #update by 1/4 of the hamiltonian squared per site
            E1 += (0.25*H_squared)/(matrix_size*1)
             
            #energy(previous function output)
            term = energy_1D(my_matrix, matrix_size, J, h, k)
             
            #specific heat
            Cv = (E1 - term**2)/(k*Temp**2)
    return Cv



