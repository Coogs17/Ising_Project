#!/usr/bin/env python
#Peter Coogan - 15325377
#pcoogan@tcd.ie

import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import random as rm
import Metropolis_algorithm as metro
import Initial_Ising as Isi







figure1 = plt.figure().add_subplot(111)
figure2 = plt.figure().add_subplot(111)
Temparray = np.arange(0.1, 4.1, 0.05)
for mag_T in Temparray:
	my_mag_matrix = metro.Metrop(Ising_matrix, times, matrix_size, J, h, mag_T, k) #Ising model over the range of temperatures
	m = mag(my_mag_matrix, matrix_size)#magnetise the matrix
	figure1.plot(mag_T, m, 'ro')
	figure1.set_title('Magnetisation versus Temperature')
	figure2.plot(mag_T, (1-m**2)/(k*mag_T), 'ro')
	figure2.set_title('Magnetic Susceptibility versus Time')

figure1.set_ylim(-0.1, 1.1)
figure1.set_xlabel('Temperature[J/K_B]')
figure1.set_ylabel('Magnetism')
figure2.set_xlabel('Temperature[J/K_B]')
figure2.set_ylabel('Magnetic Susceptibility')
plt.show()
