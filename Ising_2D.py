#!/usr/bin/env python
#Peter Coogan - 15325377
#pcoogan@tcd.ie

import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import random as rm

##########################################
'''2D Functions'''
##########################################

'''Change in Hamiltonian - also calculates nearest neighbours'''
def dH_2D(my_matrix, x, y, matrix_size, J, h):
    return -(2*J*my_matrix[x,y]*(my_matrix[x, (y+1) %matrix_size]
          + my_matrix[x, (y-1) %matrix_size] + my_matrix[(x+1) %matrix_size, y]
          + my_matrix[(x-1) %matrix_size, y])) - 2*h*my_matrix[x,y]



'''Metropolis Algorithm'''
def Metrop_2D(my_matrix, times, matrix_size, J, h, Temp, k):
    i = 0.0
    
    #iterate over the number of sweeps
    for i in range(times):
        i+=1
        
        #iterate over all rows in the matrix
        for x in range(0, matrix_size):
            
            #iterate over all columns in the matrix
            for y in range(0, matrix_size):
                
                #conditions to flip a spin
                if (dH_2D(my_matrix, x, y, matrix_size, J, h))>0:
                    
                    #Spins flip if change is greater than 0
                    my_matrix[x,y] *= -1
                    
                else:
                    
                    #create a random number between 0 and 1
                    num = rm.random()
                    
                    #flip the spin if the term is greater than the number
                    if num < (np.exp(dH_2D(my_matrix, x, y, matrix_size, J, h)
                    /(k*Temp))):
                        
                        #spins flip
                        my_matrix[x,y] *= -1
    return my_matrix

'''Magnetisation'''
def mag_2D(my_matrix, matrix_size):
    
    #average of sum of all spin values
    return np.abs((np.sum(my_matrix)))/(matrix_size*matrix_size)

'''Magnetic Susceptibility'''
def mag_2D_suscept(my_matrix, matrix_size, Temp, k):
    
    #square of the average of sum of all spin values
    mag1 = (mag_2D(my_matrix, matrix_size))**2
    
    #average of the sum of the square of all spin values
    mag2 = np.abs((np.sum(my_matrix*my_matrix)))/(matrix_size*matrix_size)
    
    #susceptibility
    return (mag2 - mag1)/Temp
    
	
'''Average Energy per site'''
def energy_2D(my_matrix, matrix_size, J, h, k):
    
    #initial value
    E = 0.0
    
    #iterate over all rows
    for x in range(0, matrix_size):
        
        #iterate over all columns
        for y in range(0, matrix_size):
            
             #Hamiltonian
             H = -(J*my_matrix[x,y]*(my_matrix[x, (y+1) %matrix_size]
             + my_matrix[x, (y-1) %matrix_size]
             + my_matrix[(x+1) %matrix_size, y]
             + my_matrix[(x-1) %matrix_size, y])) - h*my_matrix[x,y]
             
             #energy is updated by half of the hamiltonian per site
             E += (0.5*H)/(matrix_size*matrix_size)
    return E        
            
'''Specific Heat'''
def sheat_2D(my_matrix, matrix_size, J, h, Temp, k):
    
    #initial value
    E1 = 0.0
    
    #iterate over all rows
    for x in range(0, matrix_size):
        
        #iterate over all columns
        for y in range(0, matrix_size):
            
             #Hamiltonian squared
             H_squared = (-((J*my_matrix[x,y]*(my_matrix[x, (y+1) %matrix_size]
             + my_matrix[x, (y-1) %matrix_size]
             + my_matrix[(x+1) %matrix_size, y]
             + my_matrix[(x-1) %matrix_size, y])) - h*my_matrix[x,y]))**2
             
             #update by 1/4 of the hamiltonian squared per site
             E1 += (0.25*H_squared)/(matrix_size*matrix_size)
             
             #energy(previous function output)
             term = energy_2D(my_matrix, matrix_size, J, h, k)
             
             #specific heat
             Cv = (E1 - term**2)/(k*Temp**2)
    return Cv


