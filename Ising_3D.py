
#!/usr/bin/env python
#Peter Coogan - 15325377
#pcoogan@tcd.ie

import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import random as rm

###########################
'''3D Functions'''
###########################

'''Update nearest neighbours'''
def cubic(my_matrix, matrix_size, x, y, z):
    right = my_matrix[(x+1) %matrix_size, y, z]
    left = my_matrix[(x-1) %matrix_size, y, z]
    up = my_matrix[x, (y+1) %matrix_size, z]
    down = my_matrix[x, (y-1) %matrix_size, z]
    forward = my_matrix[x, y, (z+1) %matrix_size]
    back = my_matrix[x, y, (z-1) %matrix_size]
    return right + left + up + down + forward + back


'''Change in Hamiltonian'''
def dH_3D(my_matrix, matrix_size, x, y, z, J, h):
    return (-(2*J*my_matrix[x,y,z]*cubic(my_matrix, matrix_size, x, y, z))
    - 2*h*my_matrix[x,y,z])
    
'''Metropolis Algorithm'''
def Metrop_3D(my_matrix, times, matrix_size, J, h, Temp, k):
    i = 0.0
    
    #iterate over then number of sweeps
    for i in range(times):
        i += 1
        
        #iterate over all rows
        for x in range(0, matrix_size):
            
            #iterate over all columns
            for y in range(0, matrix_size):
                
                #iterate over all layers
                for z in range(0, matrix_size):
                    
                    #conditions to flip a spin
                    if dH_3D(my_matrix, matrix_size, x, y, z, J, h) > 0:
                        
                        #spins flip if change is greater than 0
                        my_matrix[x,y,z] *= -1
                    else:
                        
                        #create a random number between 0 and 1
                        num = rm.random()
                        
                        #flip spins if the term is greater than the number
                        if num < (np.exp((dH_3D(my_matrix, matrix_size, x, y, z, J, h))/(k*Temp))):
                            
                            #spins flip
                            my_matrix[x,y,z] *= -1
                            
    return my_matrix


'''Magnetisation'''
def mag_3D(my_matrix, matrix_size):
    
    #average of sum of all spin values
    return np.abs((np.sum(my_matrix)))/(matrix_size*matrix_size*matrix_size)

'''Magnetic Susceptibility'''
def mag_3D_suscept(my_matrix, matrix_size, Temp, k):
    
    #square of the average of sum of all spin values
    mag1 = (mag_3D(my_matrix, matrix_size))**2
    
    #average of the sum of the square of all spin values
    mag2 = np.abs((np.sum(my_matrix*my_matrix)))/(matrix_size*matrix_size*matrix_size)
    
    #susceptibility
    return (mag2 - mag1)/Temp



'''Average Energy per site'''
def energy_3D(my_matrix, matrix_size, J, h, k):
    
    #initial value
    E = 0.0
    
    #iterate over all rows
    for x in range(0, matrix_size):
        
        #iterate over all columns
        for y in range(0, matrix_size):
            
            #iterate over all layers
            for z in range(0, matrix_size):
            
                #Hamiltonian
                H = (dH_3D(my_matrix, matrix_size, x, y, z, J, h))/2
            
                #energy is updated by half of the hamiltonian per site
                E += (0.5*H)/(matrix_size*matrix_size*matrix_size)
    return E

'''Specific Heat'''
def sheat_3D(my_matrix, matrix_size, J, h, Temp, k):
    
    #initial value
    S = 0.0
    
    #iterate over all rows
    for x in range(0, matrix_size):
        
        #iterate over all columns
        for y in range(0, matrix_size):
            
            #iterate over all layers
            for z in range(0, matrix_size):
            
                #Hamiltonian squared
                H_squared = ((dH_3D(my_matrix, matrix_size, x, y, z, J, h))
                /2)**2
            
                #update by 1/4 of the hamiltonian squared per site
                S += (0.25*H_squared)/(matrix_size*matrix_size*matrix_size)
            
                #energy(previous function output)
                term = energy_3D(my_matrix, matrix_size, J, h, k)
            
                #specific heat
                Cv = (S - term**2)/(k*Temp**2)
    return Cv







