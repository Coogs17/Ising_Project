#!/usr/bin/env python
#Peter Coogan - 15325377
#pcoogan@tcd.ie

import numpy as np
import scipy as sp
import matplotlib.pylab as plt
import random as rm
import Metropolis_algorithm as metro

#define constants
matrix_size = 20#dimensions
J = 1#exchange energy
h = 0#magnetic field
times = 100#iterations
#Temperature = 2.2#temperature
k = 1#boltzmann

class lattice_mod:#initialise the matrix
	def __init__(self, matrix_size):
		self.matrix_size = matrix_size
		self.lattice_2D()

	def lattice_2D(self):
		self.lattice = np.random.choice([1, -1], (matrix_size, matrix_size))
	
		
	
my_lattice_initial = lattice_mod(matrix_size).lattice


'''print metro.Metrop_2D(my_lattice.lattice, times, matrix_size, J, h, Temp, k)
initial_plot = plt.figure().add_subplot(111)
initial_plot.imshow(metro.Metrop_2D(my_lattice.lattice, times, matrix_size, J, h, Temp, k), cmap = 'hot', interpolation = 'none')
initial_plot.set_title('Domain Formation')
'''

#figure1 = plt.figure().add_subplot(111)
figure2 = plt.figure().add_subplot(111)
Temparray = np.arange(0.1, 4.1, 0.1)
for mag_T in Temparray:
	my_lattice_final = lattice_mod(matrix_size).lattice
	##my_mag_matrix = metro.Metrop_2D(my_lattice_final.lattice, times, matrix_size, J, h, mag_T, k) #Ising model over the range of temperatures
	my_mag_sus_matrix = metro.Metrop_2D(my_lattice_final, times, matrix_size, J, h, mag_T, k)
	#m = metro.mag_2D(my_mag_matrix, matrix_size)#magnetise the matrix
	m_s = metro.mag_2D_suscept(my_mag_sus_matrix, matrix_size, mag_T, k)
	#figure1.plot(mag_T, m, 'ro')
	#figure1.set_title('Magnetisation versus Temperature')
	figure2.plot(mag_T, (m_s), 'ro')
	figure2.set_title('Magnetic Susceptibility versus Time')

#figure1.set_ylim(-0.1, 1.1)
#figure1.set_xlabel('Temperature[J/K_B]')
#figure1.set_ylabel('Magnetism')
figure2.set_xlabel('Temperature[J/K_B]')
figure2.set_ylabel('Magnetic Susceptibility')
plt.show()

plt.show()


