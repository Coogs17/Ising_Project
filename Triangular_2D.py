#!/usr/bin/env python
#Peter Coogan - 15325377
#pcoogan@tcd.ie

import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import random as rm
import math

###############################
'''2D Triangular Functions'''
###############################


'''Update Nearest Neighbours'''
def triangular(my_matrix, matrix_size, x, y):
    right = my_matrix[(x+1) %matrix_size, y]
    left = my_matrix[(x-1) %matrix_size, y]
    up = my_matrix[x, (y+1) %matrix_size]
    down = my_matrix[x, (y-1) %matrix_size]
    if (x % 2) == 0:
        
        #two extra neighbours
        trans1 = my_matrix[(x+1) %matrix_size, (y+1) %matrix_size]
        trans2 = my_matrix[(x+1) %matrix_size, (y-1) %matrix_size]
        
    else:
        
        #two extra neighbours
        trans1 = my_matrix[(x-1) %matrix_size, (y-1) %matrix_size]
        trans2 = my_matrix[(x-1) %matrix_size, (y+1) %matrix_size]
        
    return right + left + up + down + trans1 + trans2




'''Change in Hamiltonian'''
def dH_triangular(my_matrix, x, y, matrix_size, J, h):
    return (-(2*J*my_matrix[x,y]*(triangular(my_matrix, matrix_size, x, y)))
    - 2*h*my_matrix[x,y])
#Multiply the second term by cos(theta) to create a lattice of non collinear spins. Using theta = pi/3 yields a ground state energy of -1.5J

'''Metropolis Algorithm'''
def Metrop_triangular(my_matrix, times, matrix_size, J, h, Temp, k):
    i = 0.0
    
    #iterate over the number of sweeps
    for i in range(times):
        i += 1
        
        #iterate over all rows
        for x in range(0, matrix_size):
            
            #iterate over all columns
            for y in range(0, matrix_size):
                
                #conditions to flip a spin
                if dH_triangular(my_matrix, x, y, matrix_size, J, h) > 0:
                    
                    #spins flip if change is greater than 0
                    my_matrix[x,y] *= -1
                else:
                    
                    #create a random number between 0 and 1
                    num = rm.random()
                    
                    #flip spins if the term is greater than the number
                    if num < (np.exp((dH_triangular(my_matrix, x, y, matrix_size, J, h))/(k*Temp))):
                        
                        #spins flip
                        my_matrix[x,y] *= -1
                    
    
    return my_matrix


#THIS IS A 2D LATTICE - 2D MAGNETISATION AND SUSCEPTIBILITY FUNCTIONS WILL
#SUFFICE FOR THIS CASE - NO NEED TO DEFINE NEW FUNCTIONS


'''Average Energy per site'''
def energy_triangular(my_matrix, matrix_size, J, h, k):
    
    #initial value
    E_t = 0.0
    
    #iterate over all rows
    for x in range(0, matrix_size):
        
        #iterate over all columns
        for y in range(0, matrix_size):
            
            #Hamiltonian
            H = (dH_triangular(my_matrix, x, y, matrix_size, J, h))/2
            
            #energy is updated by half of the hamiltonian per site
            E_t += (0.5*H)/(matrix_size*matrix_size)
    return E_t

'''Specific Heat'''
def sheat_triangular(my_matrix, matrix_size, J, h, Temp, k):
    
    #initial value
    S_t = 0.0
    
    #iterate over all rows
    for x in range(0, matrix_size):
        
        #iterate over all columns
        for y in range(0, matrix_size):
            
            #Hamiltonian squared
            H_squared = ((dH_triangular(my_matrix, x, y, matrix_size, J, h))
            /2)**2
            
            #update by 1/4 of the hamiltonian squared per site
            S_t += (0.25*H_squared)/(matrix_size*matrix_size)
            
            #energy(previous function output)
            term = energy_triangular(my_matrix, matrix_size, J, h, k)
            
            #specific heat
            Cv = (S_t - term**2)/(k*Temp**2)
    return Cv
